Solus does not package `premake5` (which is why it's on this repo). Unfortunately, this means it cannot be specified as a build dependency.

There is no way to trick `solbuild` into using it that I've found. Fortunately, it doesn't have any other dependencies, so it's easy to just build outside the environment.
