# solus
Collection of packages that haven't been submitted to the repos. As most of them are attempts at getting them to build on Solus, a few do not build:

- `kitty` depends on a version of `glew` that Solus does not package;
- `onedrived` has a fuckton of Python libraries that Solus doesn't package;
- `otfcc` depends on `premake5` (in this repo), and `solbuild` won't install dependencies not in the repos;
- `qthreads` depends on `hwloc`, and `chapel` may depend on both. As such, `solbuild` won't build these.

Some workarounds had to be taken in getting them to build:
- `ttfautohint` does not come with the GUI, as the "test QT application" in the configure script fails to build.
